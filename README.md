# Scripts for Tracker Calibrations

## Bias Scan
`biasScan/cmsTime`
 
Usage: `cmsTime [HV Value]`. This will produce output files called `timeLog` (with all runs in the directory) as well as `timeLog_[HV Value]` for each HV Value. Note that if you run the same HV value twice, it will just be appended to the appropriate files. 

Each line in the log file contains an index, timestamp (up to seconds), the run number, event number, and number of events processed.
